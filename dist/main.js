var app = angular.module('app', []);

app.controller('mainController', ['$scope', '$http', function ($scope, $http) {

	function updateData(value) {
		if (typeof value == 'undefined') {
			return;
		}

		if (value === null) {
			$scope.outData = $scope.data;
			var devices = 0;
			var channels = 0;
			for (var key in $scope.data) {
				devices += $scope.data[key].Devices;
				channels += $scope.data[key].Channels;
			}
			$scope.totalDevices = devices;
			$scope.totalChannels = channels;
		}
		else {
			$scope.outData = {};
			$scope.outData[value] = $scope.data[value];
			$scope.totalDevices = $scope.data[value].Devices;
			$scope.totalChannels = $scope.data[value].Channels;
		}
		$scope.keysSorted = Object
			.keys($scope.outData)
			.sort(function(a,b) {
				var channelSort = ($scope.outData[b].Channels)
					-($scope.outData[a].Channels);
				return channelSort === 0 ? ($scope.outData[b].Devices)
					-($scope.outData[a].Devices) : channelSort;
			})
	}

    $http.get('data.json')
		.then(function successCallback(response) {
			$scope.data = response.data;
			updateData(null);
	});

	$scope.$watch('dropdownSelection',function (newValue, oldValue, scope) {
		updateData(newValue);
	})

}]);

