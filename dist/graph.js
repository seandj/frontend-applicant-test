app.directive("graph", function() {
	 return {
		restrict: 'E',
		templateUrl: 'dist/graph.html',
		replace: true,
		scope: {
					 city: "@",
					 channels: "@",
					 devices: "@",
				},
		link: function(scope, elem, attr) {
			var channelWidth = scope.channels;
			if (channelWidth < 40) {channelWidth = 40;}
			var channelClass = elem[0].getElementsByClassName("channels");
			channelClass[0].style.width = channelWidth + "px";

			var deviceWidth = scope.devices;
			if (deviceWidth < 40) {deviceWidth = 40;}
			var deviceClass = elem[0].getElementsByClassName("devices");
			deviceClass[0].style.width = deviceWidth + "px";
		},
	 }
});
